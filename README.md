# Automatic Privacy Project Documentation

* [Architecture](architecture.md)
* [Database Schema](database.md)
* [Data Flow](data-flow.md)
* [Developer Setup](developer-setup.md)
* [User Setup](user-setup.md)
* [Miscellaneous](misc.md)

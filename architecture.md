# Application Architecture

## Overview
The purpose of our application is to collect driving data from the Automatic Pro Adapter (an adapter that plugs in to the OBD2 port in a vehicle), process that data in interesting ways to derive additional data and then display it to users. Our application is built using the "MERN" (MongoDB, Express, React, Node.js) stack. It is split into two parts, a frontend developed primarily with the React library and a backend developed primarily using the Express framework. After a user has authenticated with our application, their data is collected via the Automatic Pro Adapter in the their vehicle and is then sent to our backend server via Automatic's webhook API. Our backend server accepts that data and stores it in a MongoDB (NoSQL style database) instance. The frontend retrieves the data from the backend by requesting specific Express routes. On request, the backend will process the data and return it to the frontend in JSON format. The frontend then renders a webpage with the data viewable in various formats such as charts and graphs.

## Hardware
* ### Desktop Server and Operating System
    Our web application is running on an HP Pavilion HPE Desktop with a Core i7 Intel processor. The operating system is Ubuntu 16.04 LTS (GNU/Linux 4.4.0-31-generic x86_64).

* ### Automatic Pro Adapter
    The Automatic Pro Adapter is an adapter sold by Automatic Labs. It plugs in to the OBD2 port on a vehicle to collect driving data about that vehicle. It has a smartphone companion app for setup and to view various driving metrics. The device polls the users latitude, longitude coordinates every 20 seconds using a 3G connection.

## Software
* ### NGINX Reverse Proxy
    We use NGINX as a reverse proxy to direct web traffic to our application. For instance, when a user navigates to our application's designated URL, NGINX directs those web requests to the specific port in which our application is running. This allows us to host multiple applications on different ports the same machine, like we do with our frontend and backend.

* ### Node.js
    Node.js is a JavaScript runtime meant for building web based applications. It's capable of handling web requests (GET, POST, PUT, DELETE, etc.) for the backend as well as render React for the frontend. Many REST APIs use JSON format to send a receive data. Since Node.js is JavaScript based, JSON data is supported out of the box. Our choice of Node.js was due to its flexibility and web based nature. Both our frontend and backend use Node.js.

    * #### Express
        Express is a framework for Node.js that makes it straightforward to handle routing in a web application. According to Express, "Routing refers to how an application’s endpoints (URIs) respond to client requests." Express is what we use to define our endpoints (the URL plus the identifer) and tell our application what to do when those endpoints are accessed. For example, we have a route (endpoint) that we specify to Automatic to receive a POST request with a user's data.

    * #### React
        React is a JavaScript library that was created to build user interfaces. Our frontend is written in React. React was chosen for our frontend because its wide popularity has led to there being tons of documentation, tools, and tutorials for building applications with it. React allows us to build individual pieces, called components, for each part of our interface. This modular approach makes it easier to build an intuitive user interface that can be adjusted later if needed.

        * ##### create-react-app
            This is a tool that is provided by React that creates the necessary boilerplate for a React app. It also includes scripts to run the application locally as well as build it into a static `build` directory that can be deployed on our server. It also has built-in support for environment variables. More info about environment variables can be found in [misc.md](misc.md).

    * #### PM2
        [PM2](http://pm2.keymetrics.io/) is a Node.js process manager that we use to run our frontend and backend applications on the server as well as deploy an updated version. It also allows us to specify environments such as development and production and allows separate deployments for each environment.

    * #### Nodenv
        [Nodenv](https://github.com/nodenv/nodenv) is a Node.js version manager. This allows a developer to have multiple versions of Node installed and use different versions for different projects. The command `nodenv install x.xx.xx` where x.xx.xx is the Node.js version installs the specified version of Node.js. To specify a project use a specific version of Node.js, navigate to the root directory of the project and use the command `nodenv local x.xx.xx`. This adds a `.node-version` file to the working directory specifying which version of Node.js to use.

* ### MongoDB
    Our database of choice is MongoDB. It is a NoSQL style database, which means the schema is more flexible than a relational table structure. The data we're working with is in the JSON format which is also how MongoDB stores data, meaning there is minimal processing to store the data coming from Automatic. It is also straightforward to retrieve data from the database for the same reason. We use the Mongoose Node.js package to interact with our MongoDB instance.

* ### Automatic Developer Apps Manager
    The Automatic Developer Apps Manager provides us with a Client ID and Secret which gives access to the Automatic REST and Webhook APIs. It also allows us to configure the URL to redirect a user after authenticating with Automatic. Because of this, we need to have 2 Client ID and Secret pairs - one for running our application locally, and one for running our application on the server. Depending on which Client ID and Secret pair are used, Automatic will either direct the browser back to an instance of application running locally or the instance of application running on our server.
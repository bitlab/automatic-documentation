# Database Structure

We use a MongoDB instance to store the data our application receives from the Automatic Webhook API. The types of events that trigger Automatic to send data to our application are: hard acceleration, hard brake, ignition on, ignition off, speeding, MIL (Malfunction Indicator Light) on, MIL off, and trip finished. Our MongoDB instance has a collection for each of these types of events. After receiving a JSON payload from one of these events, the only initial processing our backend does is replace the `id` field name with `_id` because `_id` is the default unique identifier that MongoDB uses. This ensures that duplicate entries are not stored in a collection.

We also maintain a User collection that stores the user's Automatic user ID, an access token for our application to make requests to the Automatic API for that user's information. Similarly to the stored events, we replace the name of the `id` field with `_id` in the User collection. A user will not be entered in the User collection of our database unless they approve through their Automatic account.

The following are the schemas for each type of event as well as the user collection:

## Hard Acceleration

```
    {
        _id: String,
        user: {
          id: String,
          v2_id: String,
        },
        type: String,
        created_at: Number,
        time_zone: String,
        location: {
          lat: Number,
          lon: Number,
          accuracy_m: Number,
          ts: Number
        },
        vehicle: {
          id: String,
          year: String,
          make: String,
          model: String,
          color: String,
          display_name: String
        },
        device : {
          id: String
        },
        g_force: Number
      }
```

## Hard Brake

```
    {
        _id: String,
        user: {
          id: String,
          v2_id: String,
        },
        type: String,
        created_at: Number,
        time_zone: String,
        location: {
          lat: Number,
          lon: Number,
          accuracy_m: Number,
          ts: Number
        },
        vehicle: {
          id: String,
          year: String,
          make: String,
          model: String,
          color: String,
          display_name: String
        },
        device : {
          id: String
        },
        g_force: Number
      }
```

## Ignition On

```
    {
        _id: String,
        user: {
          id: String,
          v2_id: String,
        },
        type: String,
        created_at: Number,
        time_zone: String,
        location: {
          lat: Number,
          lon: Number,
          accuracy_m: Number,
          ts: Number
        },
        vehicle: {
          id: String,
          year: String,
          make: String,
          model: String,
          color: String,
          display_name: String
        },
        device : {
          id: String
        }
      }
```

## Ignition Off

```
    {
        _id: String,
        user: {
          id: String,
          v2_id: String,
        },
        type: String,
        created_at: Number,
        time_zone: String,
        location: {
          lat: Number,
          lon: Number,
          accuracy_m: Number,
          ts: Number
        },
        vehicle: {
          id: String,
          year: String,
          make: String,
          model: String,
          color: String,
          display_name: String
        },
        device : {
          id: String
        }
      }
```

## Speeding

```
    {
        _id: String,
        user: {
          id: String,
          v2_id: String,
        },
        type: String,
        created_at: Number,
        time_zone: String,
        location: {
          lat: Number,
          lon: Number,
          accuracy_m: Number,
          ts: Number
        },
        vehicle: {
          id: String,
          year: String,
          make: String,
          model: String,
          color: String,
          display_name: String
        },
        device : {
          id: String
        },
        speed_mph: Number
      }
```

## MIL On

```
    {
        _id: String,
        user: {
          id: String,
          v2_id: String,
        },
        type: String,
        created_at: Number,
        time_zone: String,
        location: {
          lat: Number,
          lon: Number,
          accuracy_m: Number,
          ts: Number
        },
        vehicle: {
          id: String,
          year: String,
          make: String,
          model: String,
          color: String,
          display_name: String
        },
        device : {
          id: String
        },
        dtcs: [
            {
              code: String,
              description: String,
              start: Number
            }
          ]
      }
```

## MIL Off

```
    {
        _id: String,
        user: {
          id: String,
          v2_id: String,
        },
        type: String,
        created_at: Number,
        time_zone: String,
        location: {
          lat: Number,
          lon: Number,
          accuracy_m: Number,
          ts: Number
        },
        vehicle: {
          id: String,
          year: String,
          make: String,
          model: String,
          color: String,
          display_name: String
        },
        device : {
          id: String
        },
        dtcs: [
            {
              code: String,
              description: String,
              start: Number
            }
          ],
          user_cleared: Boolean
      }
```

## Trip Finished

Note that the "path" field is an [Encoded Polyline](https://developers.google.com/maps/documentation/utilities/polylinealgorithm). You can decode a polyline here: [https://developers.google.com/maps/documentation/utilities/polylineutility](https://developers.google.com/maps/documentation/utilities/polylineutility).

```
    {
        _id: String,
        user: {
          id: String,
          v2_id: String,
        },
        type: String,
        created_at: Number,
        time_zone: String,
        location: {
          lat: Number,
          lon: Number,
          accuracy_m: Number
        },
        vehicle: {
          id: String,
          year: String,
          make: String,
          model: String,
          color: String,
          display_name: String
        },
        device : {
          id: String
        },
        trip: {
          id: String,
          start_location: {
            name: String,
            display_name: String,
            lat: Number,
            lon: Number,
            accuracy_m: Number,
            ts: Number,
            city: String,
            country: String,
            state: String,
            street_name: String,
            street_number: String,
            venue: Array,
            category: Array
          },
          start_time: Number,
          start_time_zone: String,
          end_location: {
            name: String,
            display_name: String,
            lat: Number,
            lon: Number,
            accuracy_m: Number,
            ts: Number,
            city: String,
            country: String,
            state: String,
            street_name: String,
            street_number: String,
            venue: Array,
            category: Array
          },
          end_time: Number,
          end_time_zone: String,
          path: String,
          distance_m: Number,
          hard_accels: Number,
          hard_brakes: Number,
          duration_over_Number_s: Number,
          duration_over_Number_s: Number,
          duration_over_Number_s: Number,
          fuel_cost_usd: Number,
          fuel_volume_gal: Number,
          average_mpg: Number,
          idling_time_s: Number,
          score_events: Number,
          score_speeding: Number,
          city_fraction: Number,
          highway_fraction: Number,
          night_driving_fraction: Number
        }
      }
```
# New Developer Setup

## Access

1. Developer should have access to the BITLab Group on MSU Gitlab (gitlab.msu.edu)
2. Have Rick Wash set the developer up with SSH access to the automatic user account on the futomaki server by adding the developer's public SSH key to the list of authorized users on the futomaki server (~/.ssh/authorized_keys). Running `cat ~/.ssh/id_rsa.pub | pbcopy` in the terminal will copy the user's public SSH key to the clipboard so that it can be pasted and then sent to Rick.

Side note: SSH access is not available from off of the MSU campus without using the MSU VPN. More information about access to the MSU VPN can be found here: https://tech.msu.edu/network/network-systems/firewalls-vpn/.

## Local Development for Backend and Frontend

As discussed in the [data flow](data-flow.md) doc, our application is made up of multiple parts working together. When making changes to the application, one can run a local copy of the backend and frontend to be sure that changes work as intended before pushing to version control and re-deploying on the futomaki server.

The README files in the backend (automatic-listener) and the frontend (automatic-front-end) discuss cloning, running, and deploying each separately.

To ensure privacy of user data (even pilot/test user data), developers should not use a local MongoDB instance. Instead, developers should port forward their local MongoDB port to the futomaki server over SSH so that even when doing local development, the application is accessing data remotely and the developer is not storing potentially sensitive data on their local development machine. This port forwarding can be done by running `ssh -L 27017:localhost:27017 automatic@futomaki.cas.msu.edu`. Run this command in it's own terminal window and leave it in the background during development. This makes port 27017 (the default MongoDB port) on the futomaki server accessible via port 27017 on the local development machine, thus making the local application treat the remote MongoDB instance as a local one without actually storing potentially sensitive data on the developer's local development machine. Proper setup of SSH keys as discussed in the Access section above is required for this to work properly.

If the developer is doing only frontend development, it is possible to use the deployed backend rather than a local copy. There are further instructions in the automatic-front-end README `.env` section.

## Node.js and Nodenv

Nodenv is discussed in [architecture.md](architecture.md). It makes the Node.js installation process much easier and prevents the developer from having to deal with managing multiple installed versions of Node.js manually. It is used to install a specific version of Node.js using `nodenv install x.xx.xx` where x.xx.xx is the Node.js version installs the specified version of Node.js. There is already a `.node-version` file in the backend (automatic-listener) and frontend (automatic-front-end) repositories, so this should make the setup process very smooth and allow the developer to get set up quickly and easily.

## Administrative Tasks

It is useful to have a MongoDB viewer such as Studio 3T or Robo 3T from [robomongo](https://robomongo.org/) to manually interact with the MongoDB instance running on the futomaki server. When a new user is created (details in [user-setup.md](user-setup.md)), they are initially not an admin user. However, to make this user an admin, the developer can access the users collection in the MongoDB instance running on the futomaki server and change the `is_admin` boolean value from false to true.

Similarly, an initial flag value is set to "A" when a user is created, but to change this, the developer can access the users collection in the MongoDB instance running on the futomaki server and change the `flag` value to "B".

Side note: Studio 3T is paid software, but can be used for free with an education license. Details here: https://studio3t.com/discount/education/.